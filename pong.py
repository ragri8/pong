import os
from random import randint
import pygame
from math import sin, cos, pi

pygame.init()
display_width = 1280
display_height = 720


black = (0, 0, 0)
pale_grey = (200, 200, 200)
white = (255, 255, 255)
ball_size = 32
ball_sprite = pygame.image.load("{}/img/ball.png".format(os.path.dirname(__file__)))
textFont = pygame.font.Font("freesansbold.ttf", 60)


fps = 120
bar_height = 200
gameDisplay = pygame.display.set_mode((display_width, display_height))
clock = pygame.time.Clock()
pygame.display.set_caption("Pong")


def text_objects(text):
    textSurface = textFont.render(text, True, pale_grey)
    return textSurface, textSurface.get_rect()


def display_text(text, x, y):
    TextSurf, TextRect = text_objects(text)
    TextRect = (x, y)
    gameDisplay.blit(TextSurf, TextRect)


class Game:
    def __init__(self):
        self.end = False
        self.player1 = Player(1, bar_height)
        self.player2 = Player(2, bar_height)
        self.ball = Ball()
        self.wait = fps
        self.winner = randint(1, 2)
        self.is_finished = False

    def loop(self):
        while not self.end:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.end = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_w:
                        self.player1.up(True)
                    if event.key == pygame.K_s:
                        self.player1.down(True)
                    if event.key == pygame.K_UP:
                        self.player2.up(True)
                    if event.key == pygame.K_DOWN:
                        self.player2.down(True)
                    if event.key == pygame.K_r:
                        self.reset()
                    if event.key == pygame.K_1:
                        self.change_difficulty(1)
                    if event.key == pygame.K_2:
                        self.change_difficulty(2)
                    if event.key == pygame.K_3:
                        self.change_difficulty(3)
                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_w:
                        self.player1.up(False)
                    if event.key == pygame.K_s:
                        self.player1.down(False)
                    if event.key == pygame.K_UP:
                        self.player2.up(False)
                    if event.key == pygame.K_DOWN:
                        self.player2.down(False)
            self.player1.moving()
            self.player2.moving()
            if self.wait > 0:
                self.wait -= 1
            elif self.is_finished:
                continue
            else:
                if self.is_out():
                    self.restart()
                else:
                    new_x, new_y = self.ball.next_position()
                    new_x, new_y = self.collision(new_x, new_y)
                    self.ball.assign_position(new_x, new_y)
            self.display()
            clock.tick(fps)

    def display(self):
        gameDisplay.fill(black)
        pygame.draw.rect(gameDisplay, white, (display_width // 2 - 2, 0, 4, display_height))
        self.player1.display()
        self.player2.display()
        self.ball.display()
        self.display_points()
        if self.is_finished:
            self.display_winner()
        pygame.display.update()

    def collision(self, new_x, new_y):
        if new_x < self.player1.x_position < self.ball.x_position + ball_size:
            new_x, new_y = self.player_1_collision(new_x, new_y)
        elif self.ball.x_position < self.player2.x_position < new_x + ball_size:
            new_x, new_y = self.player_2_collision(new_x, new_y)
        new_y = self.y_collision(new_y)
        return new_x, new_y

    def y_collision(self, new_y):
        if new_y < 0:
            self.ball.reverse_y()
            self.ball.y_position = -new_y
        elif new_y > display_height - ball_size:
            self.ball.reverse_y()
            new_y = (display_height - ball_size) * 2 - new_y
        return new_y

    def player_1_collision(self, new_x, new_y):
        if self.player1.y_position < self.ball.y_position + ball_size // 2 < self.player1.y_position + self.player1.height:
            self.ball.angle = pi / 2 - pi * (self.ball.y_position + ball_size * 3 // 2 - self.player1.y_position) \
                                        / (self.player1.height + ball_size * 2)
            new_x, new_y = self.ball.next_position()
            self.ball.accelerate()
        return new_x, new_y

    def player_2_collision(self, new_x, new_y):
        if self.player2.y_position - ball_size < self.ball.y_position < self.player2.y_position + self.player2.height:
            self.ball.angle = pi / 2 + pi * (self.ball.y_position + ball_size * 3 // 2 - self.player2.y_position)\
                                       / (self.player2.height + ball_size * 2)
            new_x, new_y = self.ball.next_position()
            self.ball.accelerate()
        return new_x, new_y

    def is_out(self):
        if self.ball.x_position < - ball_size:
            self.winner = 2
            self.player2.points += 1
            if self.player2.points == 9:
                self.is_finished = True
            return True
        elif self.ball.x_position > display_width:
            self.winner = 1
            self.player1.points += 1
            if self.player1.points == 9:
                self.is_finished = True
            return True
        return False

    def restart(self):
        self.ball.restart(self.winner)
        self.wait = fps

    def reset(self):
        self.restart()
        self.player1.reset()
        self.player2.reset()
        self.winner = randint(1, 2)
        self.is_finished = False

    def display_points(self):
        p1_points = str(self.player1.points)
        p2_points = str(self.player2.points)
        display_text(p1_points, display_width // 4, display_height // 8)
        display_text(p2_points, 3 * display_width // 4, display_height // 8)

    def display_winner(self):
        text = "Player {} won".format(self.winner)
        display_text(text, display_width // 2 - 200, display_height // 3)

    def change_difficulty(self, difficulty):
        if difficulty == 1:
            self.player1.height = 250
            self.player2.height = 250
        elif difficulty == 2:
            self.player1.height = 200
            self.player2.height = 200
        elif difficulty == 3:
            self.player1.height = 150
            self.player2.height = 150
        self.reset()


class Player:
    def __init__(self, player_nbr, height=200):
        self.player_nbr = player_nbr
        if player_nbr == 1:
            self.x_position = 20
        else:
            self.x_position = display_width - 30
        self.y_position = (display_height-height) // 2
        self.height = height
        self.speed_value = 480 // fps
        self.speed = 0
        self.points = 0

    def display(self):
        pygame.draw.rect(gameDisplay, white, (self.x_position, self.y_position, 10, self.height))

    def up(self, state):
        if state:
            self.speed -= self.speed_value
        else:
            self.speed += self.speed_value

    def down(self, state):
        if state:
            self.speed += self.speed_value
        else:
            self.speed -= self.speed_value

    def moving(self):
        if self.speed < 0:
            if self.y_position + self.speed > 0:
                self.y_position += self.speed
            else:
                self.y_position = 0
        elif self.speed > 0:
            if self.y_position + self.speed < display_height - self.height:
                self.y_position += self.speed
            else:
                self.y_position = display_height - self.height

    def reset(self):
        if self.player_nbr == 1:
            self.x_position = 20
        else:
            self.x_position = display_width - 30
        self.y_position = (display_height-self.height) // 2
        self.points = 0


class Ball:
    def __init__(self):
        self.x_position = display_width // 2 - ball_size // 2
        self.y_position = display_height // 2 - ball_size // 2
        self.speed = 480 // fps
        self.angle = 0
        self.collisions = 0

    def restart(self, winner=1):
        self.x_position = display_width // 2 - ball_size // 2
        self.y_position = display_height // 2 - ball_size // 2
        self.speed = 480 // fps
        if winner == 1:
            self.angle = 0
        else:
            self.angle = pi
        self.collisions = 0

    def display(self):
        gameDisplay.blit(ball_sprite, (self.x_position, self.y_position))

    def next_position(self):
        return self.x_position + self.speed * cos(self.angle), self.y_position - self.speed * sin(self.angle)

    def reverse_y(self):
        self.angle *= -1

    def assign_position(self, new_x, new_y):
        self.x_position = new_x
        self.y_position = new_y

    def accelerate(self):
        self.collisions += 1
        if self.collisions % (fps // 30) == 0 and self.collisions <= fps // 2:
            self.speed += 1
        elif self.collisions % (fps // 15) == 0 and self.collisions > fps // 2:
            self.speed += 1


game = Game()
game.loop()

pygame.quit()
quit()
